var cards = ["ciri.png", "ciri.png", "jaskier.png", "jaskier.png", "yen.png", "yen.png", "geralt.png", "geralt.png", "triss.png", "triss.png", "iorweth.png", "iorweth.png"];

//console.log(cards);

var catchedCards = [];

var randomCards = Array.from(cards);

var random = [];

//console.log(randomCards);

function randomPosition() {
    var r = "", temp = "";
    for(i = randomCards.length-1; i >= 0; i--) {
        r = Math.floor(Math.random()*i);
        //console.log("Wylosowany indeks: ", r);

        temp = randomCards[r];
        random.push(temp);
        randomCards[r] = randomCards[i];
        randomCards[i] = temp;
        randomCards.pop();

        //console.log(randomCards);
        //console.log(random);
    }
}

function catchCardData() {
    for(i = 0; i < cards.length; i++) {
        catchedCards[i] = $('#c'+i);
        //catchedCards[i] = $(document.getElementById('c'+i)); //alternatywne rozwiązanie
    }
}

function hide2Cards(nr1, nr2) {
    $("#c" + nr1).css('opacity', '0');
    $("#c" + nr2).css('opacity', '0');

    pairsLeft--;

    if(pairsLeft == 0) {
        $("#board").html("<h1>You win!<br/>Done in " + turnCounter + " turns</h1>");
        
        $("#board").html( $('#board').html() + '<div id="button" onclick="location.reload()">Try again</div>' );
    }

    lock = false;
}

function restore2Cards(nr1, nr2) {
    $('#c' + nr1).css('background-image', 'url(img/karta.png)');
    $('#c' + nr1).addClass('card');
    $('#c' + nr1).removeClass('cardA');

    $('#c' + nr2).css('background-image', 'url(img/karta.png)');
    $('#c' + nr2).addClass('card');
    $('#c' + nr2).removeClass('cardA');

    lock = false;
}

var oneCardVisible = false;
var visibleNr;
var lock = false;
var turnCounter = 0;
var pairsLeft = 6;

function revealCard(nr) {
    var opacityValue = $('#c' + nr).css('opacity');

    //console.log('revealCard(' + nr + ')', "opacityValue = ", opacityValue);

    //potrzebny był dodatkowy warunek 'nr != visibleNr', ponieważ bez tego dało się dwa razy kliknąć jedną kartę i gra zaliczała punkt
    if(opacityValue != 0 && !lock && nr != visibleNr) {
        lock = true;

        var image = "url(img/" + random[nr] + ")";

        $('#c' + nr).css('background-image', image);
        $('#c' + nr).addClass('cardA');
        $('#c' + nr).removeClass('card');

        if(!oneCardVisible) {
            //odkryto pierwszą kartę

            oneCardVisible = true;
            visibleNr = nr;
            lock = false;
        }

        else {
            //odkryto drugą kartę

            if(random[nr] == random[visibleNr]) {
                //para

                $('#c' + nr).addClass('match');
                $('#c' + visibleNr).addClass('match');

                setTimeout(function() { hide2Cards(nr, visibleNr) }, 750);
                setTimeout(function() { removeBorderColor(nr) }, 750);
            }

            else {
                //pudło

                $('#c' + nr).addClass('miss');
                $('#c' + visibleNr).addClass('miss');

                setTimeout(function() { restore2Cards(nr, visibleNr) }, 1000);
                setTimeout(function() { removeBorderColor(nr) }, 1000);
            }

            turnCounter++;

            $('#score').html('Turn counter: ' + turnCounter);

            oneCardVisible = false;
        }
    }
}

function removeBorderColor(nr) {
    $('#c' + nr).removeClass('match');
    $('#c' + nr).removeClass('miss');

    if(!isNaN(visibleNr)) {
        $('#c' + visibleNr).removeClass('match');
        $('#c' + visibleNr).removeClass('miss');
    }
}

function drawCards() {
    $('#board').html('');

    randomPosition();

    for(i = 0; i < random.length; i++) {
        $('#board').html($('#board').html() + '<div class="card" id="c' + i +'"></div>');
    }

    $('#board').html($('#board').html() + '<div id="score">Turn counter: ' + turnCounter + '</div>');

    catchCardData();

    $(catchedCards[0]).on("click", function() { revealCard(0); });
    $(catchedCards[1]).on("click", function() { revealCard(1); });
    $(catchedCards[2]).on("click", function() { revealCard(2); });
    $(catchedCards[3]).on("click", function() { revealCard(3); });
    $(catchedCards[4]).on("click", function() { revealCard(4); });
    $(catchedCards[5]).on("click", function() { revealCard(5); });
    $(catchedCards[6]).on("click", function() { revealCard(6); });
    $(catchedCards[7]).on("click", function() { revealCard(7); });
    $(catchedCards[8]).on("click", function() { revealCard(8); });
    $(catchedCards[9]).on("click", function() { revealCard(9); });
    $(catchedCards[10]).on("click", function() { revealCard(10); });
    $(catchedCards[11]).on("click", function() { revealCard(11); });
    
}

$(window).on('load', function(){ drawCards() });
